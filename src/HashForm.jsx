import React, { Component } from 'react';
import './App.css';
import Util from 'postchain-client/src/util';
import JSZip from 'jszip';
import saveAs from 'jszip/vendor/FileSaver';

class HashForm extends Component {

    constructor(props) {
        super(props);

        this.getHash = this.getHash.bind(this);
    }

    getHash() {
        var fileInput = document.getElementById('fileInput');
        var templateDisplayArea = document.getElementById('templateDisplayArea');

        var file = fileInput.files[0];

        if (file === undefined) {
            return;
        }

        var reader = new FileReader();

        reader.onload = function (e) {
            // console.log(reader.result);
            var content = Buffer.from(reader.result);
            var hash = Util.hash256(content);
            var template = `<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
    <ASiCManifest xmlns="http://uri.etsi.org/02918/v1.2.1#" xmlns:ns2="http://www.w3.org/2000/09/xmldsig#">
        <SigReference URI="META-INF/signature.p7s" MimeType="application/x-pkcs7-signature"/>
        <DataObjectReference URI="contract.txt" MimeType="text/plain;charset=iso-8859-1">
            <ns2:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"/>
            <ns2:DigestValue>${hash.toString('hex')}</ns2:DigestValue>
        </DataObjectReference>
    </ASiCManifest>`;
            templateDisplayArea.innerText = template;

            // Hash Of Manifest
            var hashOfManifestDisplayArea = document.getElementById('hashOfManifestDisplayArea');
            hashOfManifestDisplayArea.innerText = Util.hash256(Buffer.from(template)).toString('hex');

            var zip = new JSZip();
            zip.file("contract.txt", content);
            var dir = zip.folder("META-INF");
            dir.file("Manifest.xml", template);
            zip.generateAsync({type:"blob"})
                .then(function(c) {
                    // see FileSaver.js
                    saveAs(c, "contract.asics");
                });
        }

        reader.readAsArrayBuffer(file);
    }

    render() {
        return (
            <div className="center">
                <div className="card">
                    <p>Select a file:</p>
                    <input type="file" id="fileInput" onChange={this.getHash}/>
                    <h4>Manifest: </h4>
                        {/* <input className="button" type="button" value="Display Manifest" onClick={this.getHash} /> */}
                    <div>
                        <pre id="templateDisplayArea" />
                    </div>
                        <h4>Hash Of Manifest: </h4>
                        <div>
                            <pre id="hashOfManifestDisplayArea" />
                        </div>
                </div>
            </div>
        );
    }
}

export default HashForm;
