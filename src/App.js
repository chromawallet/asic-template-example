import React, { Component } from 'react';
import './App.css';
import HashForm from './HashForm';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Detached Contract Creator (ASiC format)</h1>
        </header>
        <HashForm />
      </div>
    );
  }
}

export default App;
